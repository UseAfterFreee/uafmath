/* UseAfterFreee
 * github.com/UseAfterFreee 
 *
 * Vectors
 */

#ifndef VECTOR_H
#define VECTOR_H

#include <vector>
#include <iostream>
#include <initializer_list>

#include "angle.hpp"

struct VectorDimensionException : public std::exception
{
    const char* what() const throw ()
    {
        return "Vector dimensions do not agree";
    }

};

namespace UAFMath
{
    class VectorNd
    {
        private:
        protected:
            const unsigned int dimension;
            std::vector<float> data;
        public:
            /* Constructor */
            VectorNd(unsigned int dimension); // Zero filled Vector with size dimension
            VectorNd(std::initializer_list<float> init); // Vector with predetermined values
            VectorNd(std::vector<float> init); // Vector with predetermined values
            VectorNd(const VectorNd& v); // Copy constructor

            /* Operators */
            bool operator==(const VectorNd& v) const;
            bool operator!=(const VectorNd& v) const;

            float& operator[](unsigned int i);
            VectorNd operator+(const VectorNd& v) const;
            VectorNd operator+=(const VectorNd& v);

            VectorNd operator-(const VectorNd& v) const;
            VectorNd operator-=(const VectorNd& v);

            VectorNd operator*(const float& f) const; // Vector * scaler
            VectorNd operator*=(const float& f); // Vector * scaler
            friend VectorNd operator*(const float& f, const VectorNd& v); // Scaler * Vector (cant define inside of class needs to be a friend function)

            /* Other functions*/
            float magnitude() const; // Returns the magnitude of the vector
            void normalize(); // Normalizes in place
            float dotProduct(const VectorNd& v) const;
            angle getAngle();

            friend std::ostream& operator<<(std::ostream& os, const VectorNd& v);
    };
}

#endif
