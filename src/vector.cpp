/* UseAfterFreee
 * github.com/UseAfterFreee 
 *
 * Vectors
 */

#include "vector.hpp"
#include <cmath>
#include <sstream>

namespace UAFMath
{
    vector_base::vector_base(unsigned int dimension)
        : dimension(dimension), data(dimension, 0)
    {

    }

    vector_base::vector_base(std::initializer_list<float> init)
        : dimension(init.size()), data(init)
    {

    }

    vector_base::vector_base(std::vector<float> init)
        : dimension(init.size()), data(init)
    {

    }

    vector_base::vector_base(const vector_base& v)
        : dimension(v.dimension), data(v.data)
    {

    }

    bool vector_base::operator==(const vector_base& v) const
    {
        if (v.dimension != dimension) return false;
        for (unsigned int i = 0; i < dimension; ++i)
        {
            if (v.data[i] != data[i]) return false; 
        }
        return true;
    }

    bool vector_base::operator!=(const vector_base& v) const
    {
        return !(*this == v);
    }

    float& vector_base::operator[](unsigned int i)
    {
        if (i>dimension - 1) throw VectorDimensionException();
        return data[i];
    }

    vector_base vector_base::operator+(const vector_base& v) const
    {
        if (v.dimension != dimension) throw VectorDimensionException();
        std::vector<float> new_data(dimension);
        for (unsigned int i = 0; i < dimension; ++i)
        {
            new_data[i] = data[i] + v.data[i];
        }
        return vector_base(new_data);
    }

    vector_base vector_base::operator+=(const vector_base& v)
    {
        if (v.dimension != dimension) throw VectorDimensionException();
        for (unsigned int i = 0; i < dimension; ++i)
        {
            data[i] += v.data[i];
        }
        return *this;
    }

    vector_base vector_base::operator-(const vector_base& v) const
    {
        if (v.dimension != dimension) throw VectorDimensionException();
        std::vector<float> new_data(dimension);
        for (unsigned int i = 0; i < dimension; ++i)
        {
            new_data[i] = data[i] - v.data[i];
        }
        return vector_base(new_data);
    }

    vector_base vector_base::operator-=(const vector_base& v)
    {
        if (v.dimension != dimension) throw VectorDimensionException();
        for (unsigned int i = 0; i < dimension; ++i)
        {
            data[i] -= v.data[i];
        }
        return *this;
    }

    vector_base vector_base::operator*(const float& f) const
    {
        std::vector<float> new_data(dimension);
        for (unsigned int i = 0; i < dimension; ++i)
            new_data[i] = data[i] * f;
        return vector_base(new_data);
    }

    vector_base vector_base::operator*=(const float& f)
    {
        for (unsigned int i = 0; i < dimension; ++i)
            data[i] *= f;
        return *this;
    }

    float vector_base::magnitude() const
    {
        float sum = 0;
        for (auto itr = data.begin(); itr < data.end(); ++itr)
        {
            sum += (*itr * *itr);
        }
        return sqrt(sum);
    }

    void vector_base::normalize()
    {
        /* Pre compute magnitude */
        float mag = magnitude();
        for (auto itr = data.begin(); itr < data.end(); ++itr)
            *itr /= mag;
    }

    vector_base operator*(const float& f, const vector_base& v)
    {
        return v*f;
    }

    float vector_base::dotProduct(const vector_base& v) const
    {
        if (v.dimension != dimension) throw VectorDimensionException();
        float sum = 0;
        for (unsigned int i = 0; i<dimension; ++i)
        {
            sum += (data[i] * v.data[i]);
        }
        return sum;
    }

    angle vector_base::getAngle()
    {
        return angle::in_radians(acos(data[0]/magnitude()));
    }

    std::ostream& operator<<(std::ostream& os, const UAFMath::vector_base& v)
    {
        os.precision(2);

        os << std::fixed;
        os << "{" << v.data[0];

        for (auto itr = v.data.begin() + 1; itr<v.data.end(); ++itr)
        {
            os << ", ";
            os << *itr;
        }
        os << "}";
        return os;
    }
}
