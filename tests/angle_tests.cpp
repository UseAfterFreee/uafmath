#include <catch2/catch.hpp>
#include <cmath>

#include "angle.hpp"

TEST_CASE("Angle creation and equality checking")
{
    UAFMath::angle a = UAFMath::angle::in_degrees(90);
    UAFMath::angle b = UAFMath::angle::in_radians(M_PI / 2);

    REQUIRE(a.degrees == Approx(b.degrees));
    REQUIRE(a.radians == Approx(b.radians));
}
