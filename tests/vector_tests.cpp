#include <catch2/catch.hpp>

#include "vector.hpp"
#include <cmath>

TEST_CASE("Calculate maginitude of vector", "[vector]")
{
    SECTION("One dimension only")
    {
        UAFMath::vector v({2, 0, 0, 0});
        REQUIRE(v.magnitude() == 2);
    }
    SECTION("Two dimensions")
    {
        UAFMath::vector v({3, 4});
        REQUIRE(v.magnitude() == 5);
    }
    SECTION("Multiple dimensions")
    {
        UAFMath::vector v({2,5,3,8});
        float ans = sqrt(2 * 2 + 5 * 5 + 3 * 3 + 8 * 8);
        REQUIRE(v.magnitude() == ans);
    }
    SECTION("0 Vector")
    {
        UAFMath::vector v(5);
        REQUIRE(v.magnitude() == 0);
    }
}

TEST_CASE("Vector equality checking")
{
    SECTION("Directly out of constructor")
    {
        REQUIRE(UAFMath::vector(5) == UAFMath::vector(5));
        REQUIRE(UAFMath::vector({3, 2, 5}) == UAFMath::vector({3, 2, 5}));
    }

    SECTION("Stored on the stack")
    {
        UAFMath::vector a0(5), a1(5);
        UAFMath::vector b0({3,2,1,8,4.5}), b1({3,2,1,8,4.5});

        REQUIRE(a0 == a1);
        REQUIRE(b0 == b1);
    }

    SECTION("Not equal")
    {
        UAFMath::vector a0(50), a1(5);
        UAFMath::vector b0({3,2,1,4.5}), b1({3,2,1,8,4.5});
        UAFMath::vector c0({3,2,1,4.5}), c1({3,1,8,4.5});

        REQUIRE(a0 != a1);
        REQUIRE(b0 != b1);
        REQUIRE(c0 != c1);
    }
}

TEST_CASE("Normalization")
{
    SECTION("In place normalization of vector along axis")
    {
        UAFMath::vector v({0, 4, 0});
        v.normalize();
        REQUIRE(v.magnitude() == 1);
        REQUIRE(v == UAFMath::vector({0, 1, 0}));
    }
    SECTION("In place normalization of 2D vector")
    {
        UAFMath::vector v({3, 4});
        v.normalize();
        REQUIRE(v == UAFMath::vector({3.0/5, 4.0/5}));
    }
    SECTION("Arbitrary vector")
    {
        UAFMath::vector v({2,6,4,9});
        float mag = sqrt(2*2 + 6*6 + 4*4 + 9*9);
        v.normalize();
        REQUIRE(v == UAFMath::vector({2/mag, 6/mag, 4/mag, 9/mag}));
    }
}

TEST_CASE("Vector Addition")
{
    SECTION("out of place basic addition")
    {
        UAFMath::vector v0({3, 2.3, 5});
        UAFMath::vector v1({5, 1, 2});

        UAFMath::vector expected({8, 3.3, 7});

        REQUIRE(v0 + v1 == expected);
    }
    SECTION("Failed out of place addition different dimension")
    {
        UAFMath::vector v0({3, 2.3, 5});
        UAFMath::vector v1({5, 2, 1, 2});

        REQUIRE_THROWS_AS(v0 + v1, VectorDimensionException);
    }
    SECTION("In place vector addition")
    {
        UAFMath::vector v0({3, 2.3, 5});
        UAFMath::vector v1({5, 1, 2});

        UAFMath::vector expected({8, 3.3, 7});
        
        REQUIRE((v0 += v1) == expected); // It should return the new value
        REQUIRE(v0 == expected); // Check if v0 is updated correctly
    }
    SECTION("Failed in place addition different dimension")
    {
        UAFMath::vector v0({3, 2.3, 5});
        UAFMath::vector v1({5, 2, 1, 2});

        REQUIRE_THROWS_AS(v0 += v1, VectorDimensionException);
    }
}

TEST_CASE("Vector Subtraction")
{
    SECTION("out of place basic subtraction")
    {
        UAFMath::vector v0({3, 2.3, 5});
        UAFMath::vector v1({5, 1, 2});

        UAFMath::vector expected({-2, 1.3, 3});

        REQUIRE(v0 - v1 == expected);
    }
    SECTION("Failed out of place subtraction different dimension")
    {
        UAFMath::vector v0({3, 2.3, 5});
        UAFMath::vector v1({5, 2, 1, 2});

        REQUIRE_THROWS_AS(v0 - v1, VectorDimensionException);
    }
    SECTION("In place vector subtraction")
    {
        UAFMath::vector v0({3, 2.3, 5});
        UAFMath::vector v1({5, 1, 2});

        UAFMath::vector expected({-2, 1.3, 3});
        
        REQUIRE((v0 -= v1) == expected); // It should return the new value
        REQUIRE(v0 == expected); // Check if v0 is updated correctly
    }
    SECTION("Failed in place subtraction different dimension")
    {
        UAFMath::vector v0({3, 2.3, 5});
        UAFMath::vector v1({5, 2, 1, 2});

        REQUIRE_THROWS_AS(v0 -= v1, VectorDimensionException);
    }
}

TEST_CASE("Vector with scaler multiplication")
{
    SECTION("Positive real scaler with vector")
    {
        UAFMath::vector v({2.32, 9.8, -3.5});
        float scaler = 9.2;

        UAFMath::vector expected({21.344, 90.16, -32.2});
        REQUIRE(v * scaler == expected); 
        REQUIRE(scaler * v == expected); 
    }
    SECTION("Negative real scaler with vector")
    {
        UAFMath::vector v({2.32, 9.8, -3.5});
        float scaler = -9.2;

        UAFMath::vector expected({-21.344, -90.16, 32.2});
        REQUIRE(v * scaler == expected); 
        REQUIRE(scaler * v == expected); 
    }
    SECTION("zero-ing a vector")
    {
        UAFMath::vector v({2.32, 9.8, -3.5});
        float scaler = 0;

        UAFMath::vector expected({0, 0, 0});
        REQUIRE(v * scaler == expected); 
        REQUIRE(scaler * v == expected); 
    }
    SECTION("In place multiplication")
    {
        UAFMath::vector v({2.32, 9.8, -3.5});
        float scaler = 9.2;

        UAFMath::vector expected({21.344, 90.16, -32.2});
        REQUIRE((v *= scaler) == expected); // check if it returns the correct value
        REQUIRE(v == expected); // Check if v is updated correctly
    }
}

TEST_CASE("Dot product")
{
    SECTION("Basic dot product")
    {
        UAFMath::vector v0({2, 8, 3.1});
        UAFMath::vector v1({3.7, 1.2, -1.5});

        REQUIRE(v0.dotProduct(v1) == Approx(12.35));
        REQUIRE(v1.dotProduct(v0) == Approx(12.35));
    }
    SECTION("Dot product on two orthogonal vectors")
    {
        UAFMath::vector v0({3.2, -4.2, 5});
        UAFMath::vector v1({2.3, 5.3, 2.98});
        REQUIRE(v0.dotProduct(v1) == Approx(0));
    }
    SECTION("Dimensions do not agree");
    {
        UAFMath::vector v0({3.2, 4, -4.2, 5});
        UAFMath::vector v1({2.3, 5.3, 2.98});
        REQUIRE_THROWS_AS(v0.dotProduct(v1), VectorDimensionException);
    }
}

TEST_CASE("Modifying vector")
{
    SECTION("Changing some values")
    {
        UAFMath::vector v0({3, 9, 1});
        
        v0[0] = 2;
        v0[2] -= 3;
        v0[1] = v0[2] * 3;

        UAFMath::vector expected({2, -6, -2});

        REQUIRE(v0 == expected);
    }
    SECTION("Out of bounds")
    {
        UAFMath::vector v0({3, 9, 1});
        
        REQUIRE_THROWS_AS(v0[3] = 3, VectorDimensionException);
    }
}

TEST_CASE("Get angle of vector")
{
    UAFMath::vector v({1.23, -2.13, 5.3});
    REQUIRE(v.getAngle().degrees == Approx(77.85).epsilon(0.0001));
}
