/* UseAfterFreee
 * github.com/UseAfterFreee 
 *
 * Vectors
 */

#ifndef VECTOR_H
#define VECTOR_H

#include <vector>
#include <iostream>
#include <initializer_list>

#include "angle.hpp"

struct VectorDimensionException : public std::exception
{
    const char* what() const throw ()
    {
        return "Vector dimensions do not agree";
    }

};

namespace UAFMath
{
    class vector_base
    {
        private:
        protected:
            const unsigned int dimension;
            std::vector<float> data;
        public:
            /* Constructor */
            vector_base(unsigned int dimension); // Zero filled Vector with size dimension
            vector_base(std::initializer_list<float> init); // Vector with predetermined values
            vector_base(std::vector<float> init); // Vector with predetermined values
            vector_base(const vector_base& v); // Copy constructor

            /* Operators */
            bool operator==(const vector_base& v) const;
            bool operator!=(const vector_base& v) const;

            float& operator[](unsigned int i);
            vector_base operator+(const vector_base& v) const;
            vector_base operator+=(const vector_base& v);

            vector_base operator-(const vector_base& v) const;
            vector_base operator-=(const vector_base& v);

            vector_base operator*(const float& f) const; // Vector * scaler
            vector_base operator*=(const float& f); // Vector * scaler
            friend vector_base operator*(const float& f, const vector_base& v); // Scaler * Vector (cant define inside of class needs to be a friend function)

            /* Other functions*/
            float magnitude() const; // Returns the magnitude of the vector
            void normalize(); // Normalizes in place
            float dotProduct(const vector_base& v) const;
            angle getAngle();

            friend std::ostream& operator<<(std::ostream& os, const vector_base& v);
    };

    class vector : public vector_base
    {
        public:
            using vector_base::vector_base;
    };
}

#endif
