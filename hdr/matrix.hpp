/* UseAfterFreee
 * github.com/UseAfterFreee
 *
 * Matrices
 */

#ifndef MATRIX_H
#define MATRIX_H

#include <vector>
#include <memory>

namespace UAFMath
{
    class Matrix
    {
        private:
            const int rows, columns;
            std::vector<float> data;
        public:
            /* Constructors */
            Matrix(int rows, int columns);
            Matrix(int rows, int columns, float* init_data);
            Matrix(int rows, int columns, std::vector<float> init_data);

            /* Copy Constructor */
            Matrix(const Matrix& m);

            /* Destructor */
            ~Matrix();
    };
}

#endif
