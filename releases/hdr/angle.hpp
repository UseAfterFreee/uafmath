/* UseAfterFreee
 * github.com/UseAfterFreee
 *
 * Angles in degrees and radians
 */

#ifndef ANGLE_H
#define ANGLE_H

namespace UAFMath
{
    struct angle
    {
        float degrees;
        float radians;

        static angle in_degrees(float degrees);
        static angle in_radians(float radians);
    };
}

#endif
