#include "angle.hpp"
#include <cmath>

namespace UAFMath
{
    angle angle::in_degrees(float _degrees)
    {
        float deg = _degrees;
        float rad = _degrees * (M_PI/180);
        return angle({deg, rad});
    }

    angle angle::in_radians(float _radians)
    {
        float rad = _radians;
        float deg = _radians * (180/M_PI);
        return angle({deg, rad});
    }
}
