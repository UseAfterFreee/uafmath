UseAfterFreee Math
==================

![](https://user-images.githubusercontent.com/45241212/50545810-be219400-0c1c-11e9-8e83-f3397123e344.png)

Intro
-----

This is a math library that contains classes and functions for vector and matrix math

Disclaimer
----------

This library is in development and thus incomplete.  
As of right now the vector class and functions are for the biggest part stable and can be used.  
If you want the latest version you can compile it by hand and use all the classes inside the UAFMath namespace.

Documentation
-------------

Usage is coming soon

Credits
-------

Written by UseAfterFreee
