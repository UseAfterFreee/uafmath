/* UseAfterFreee
 * github.com/UseAfterFreee
 *
 */

#include <iostream>
#include "vector.hpp"

int main(int argc, char* argv[])
{
    std::cout << "Maths" << std::endl;
    UAFMath::vector v(4);
    std::cout << "Vector " << v << std::endl;
}
