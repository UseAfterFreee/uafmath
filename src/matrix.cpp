/* UseAfterFreee
 * github.com/UseAfterFreee
 *
 * Matrices
 */

#include "matrix.hpp"

namespace UAFMath
{
    /* Constructors */
    Matrix::Matrix(int rows, int columns)
        : rows(rows), columns(columns), data(rows*columns, 0)
    {

    }

    Matrix::Matrix(int rows, int columns, float* init_data)
        : rows(rows), columns(columns), data(rows*columns, 0)
    {
        for (int i = 0; i < rows * columns; ++i)
        {
            data[i] = init_data[i];
        }
        
    }

    Matrix::Matrix(int rows, int columns, std::vector<float> init_data)
        : rows(rows), columns(columns), data(init_data)
    {

    }

    /* Copy Constructor */
    Matrix::Matrix(const Matrix& m)
        : rows(m.rows), columns(m.columns), data(m.data)
    {
    }

    /* Destructor */
    Matrix::~Matrix()
    {

    }
}
